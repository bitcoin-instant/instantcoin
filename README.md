# Bitcoin Instant

![Bitcoin Instant](https://gitlab.com/bitcoin-instant/instantcoin/raw/0f22b481b898ae1eda8c0b31ce1fbf1291404d8e/src/qt/res/icons/toolbar.png)

## What is Bitcoin Instant?

Bitcoin Instant is an experimental new digital currency that enables instant payments to anyone, anywhere in the world. Bitcoin Instant uses peer-to-peer technology to operate with no central authority: managing transactions and issuing money are carried out collectively by the network.

## License

Bitcoin Instant is released under the terms of the MIT license. See `COPYING` for more information or see http://opensource.org/licenses/MIT.

## Build

[GNU/Linux Make Instructions](https://gitlab.com/bitcoin-instant/instantcoin/blob/master/doc/build-unix.md)

[Windows Make Instructions](https://gitlab.com/bitcoin-instant/instantcoin/blob/master/doc/build-msw.md)

[OS X Make Instruction](https://gitlab.com/bitcoin-instant/instantcoin/blob/master/doc/build-osx.md)
